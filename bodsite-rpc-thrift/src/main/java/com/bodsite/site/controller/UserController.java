package com.bodsite.site.controller;

import java.util.List;

import org.apache.thrift.TException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bodsite.common.cache.annotation.Cache;
import com.bodsite.common.constant.CacheConstant.CACHE_TYPE;
import com.bodsite.common.logger.Logger;
import com.bodsite.common.logger.LoggerFactory;
import com.bodsite.core.rest.BaseRestController;
import com.bodsite.core.rest.http.ResponseUtil;
import com.bodsite.rpc.thrift.entity.OrderUserVo;
import com.bodsite.rpc.thrift.entity.OrderVo;
import com.bodsite.rpc.thrift.entity.UserVo;
import com.bodsite.rpc.thrift.service.UserService;

@RestController
@RequestMapping(value = "user", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class UserController extends BaseRestController {
	private static final Logger logger = LoggerFactory.getLogger(UserController.class);
	@Autowired
	private UserService.Iface userService;

	@RequestMapping(value = "user", method = RequestMethod.GET)
	public String user(@RequestParam("userId") Long userId) throws TException {
		UserVo user = userService.findUser(userId);
		logger.info(" user:" + user.toString());
		return ResponseUtil.buildSuccessJsonStr(user);
	}

	//@Cache(key="c_order_list_{0}_{1}",cacheType=CACHE_TYPE.LOCAL_CACHE,exp=30)
	@RequestMapping(value = "orderList", method = RequestMethod.GET)
	public String orderList(@RequestParam(defaultValue = "0", value = "start") Integer start,
			@RequestParam(defaultValue = "20", value = "size") Integer size) throws TException {
		List<OrderVo> userList = userService.findOrderList(null, start, size);
		return ResponseUtil.buildSuccessJsonStr(userList);
	}
	
	@RequestMapping(value = "orderUserList", method = RequestMethod.GET)
	public String orderUserList(@RequestParam(defaultValue = "0", value = "start") Integer start,
			@RequestParam(defaultValue = "20", value = "size") Integer size) throws TException {
		List<OrderUserVo> userList = userService.findOrderUser(null, start, size);
		return ResponseUtil.buildSuccessJsonStr(userList);
	}

}
