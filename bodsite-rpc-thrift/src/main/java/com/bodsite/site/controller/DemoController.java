package com.bodsite.site.controller;

import org.apache.thrift.TException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bodsite.core.rest.BaseRestController;
import com.bodsite.core.rest.http.ResponseUtil;
import com.bodsite.rpc.thrift.service.HellowService;


@RestController
@RequestMapping(value = "demo", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class DemoController extends BaseRestController {

	@Autowired
	private HellowService.Iface hellowService;

	@RequestMapping(value = "hellow", method = RequestMethod.GET)
	public String demo() throws TException {
		String str = hellowService.hellowString("hellow thrift!");
		System.out.println(" client:"+str);
		return ResponseUtil.buildSuccessJsonStr(str);
	}

}
