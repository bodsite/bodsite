package com.bodsite.rpc.thrift.test;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.bodsite.rpc.thrift.service.HellowService;
import com.bodsite.rpc.thrift.service.HellowService.Iface;

public class HellowTest{
	
	public static void main(String[] args) throws Exception {
		ApplicationContext ac = new ClassPathXmlApplicationContext("spring/application-context.xml");
		HellowService.Iface ifce = (Iface) ac.getBean("hellowService");
		System.out.println(ifce.hellowString("2222"));
		HellowService.Iface ifce2 = (Iface) ac.getBean(HellowService.Iface.class);
		System.out.println(ifce.hellowString("333"));

	}
	
}
