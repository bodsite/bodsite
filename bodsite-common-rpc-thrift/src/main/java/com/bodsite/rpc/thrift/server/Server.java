package com.bodsite.rpc.thrift.server;

import java.util.Map;
import java.util.Map.Entry;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.bodsite.common.logger.Logger;
import com.bodsite.common.logger.LoggerFactory;
import com.bodsite.rpc.spring.schema.bean.Module;
import com.bodsite.rpc.spring.schema.bean.Service;
import com.bodsite.rpc.spring.schema.bean.Zookeeper;

public class Server{
	private static final Logger logger = LoggerFactory.getLogger(Server.class);
	
	public static void main(String[] args) {
		ApplicationContext ac = new ClassPathXmlApplicationContext("spring/*.xml");
		Server server = new Server();
		server.serverStart(ac);
	}
	
	/**
	 * 启动 - 注册
	 * @author bod
	 * 2017年4月12日下午1:30:50
	 */
	public void serverStart(ApplicationContext ac){
		try{
			Module module = ac.getBean(Module.class);
			Zookeeper zookeeper  =ac.getBean(Zookeeper.class);
			Map<String,Service> serviceMap = ac.getBeansOfType(Service.class);
			if(serviceMap!=null&&!serviceMap.isEmpty()){
				for(Entry<String,Service> entry:serviceMap.entrySet()){
					new ThtiftServer(entry.getValue()).start();
					registry(entry.getValue(),module,zookeeper);
				}
			}
		}catch(Exception e){
			logger.error(" server registry error ... ",e);
		}
	}
	
	/**
	 * 注册服务
	 * @author bod
	 * 2017年4月12日下午1:30:40
	 * @throws Exception 
	 * @throws InterruptedException 
	 */
	public void registry(Service service,Module module,Zookeeper zookeeper) throws InterruptedException, Exception{
		new ServiceRegistry(service,module,zookeeper).connect();
	}

}
