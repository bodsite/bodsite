package com.bodsite.rpc.thrift.constant;

public class ThriftConstant {
	
	public static final String BASE_HOME = "/bodsite";
	
	public static final String SERVICE_HOME = BASE_HOME+"/service";

}
