package com.bodsite.rpc.thrift.expetion;

import com.bodsite.common.exception.BaseException;

/**
 * rpc 调用 异常
 * 
 * @author bod 2017年4月17日上午9:45:04
 */
public class RpcException extends BaseException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public RpcException(RPC_EXPECTION ex, Throwable cause, String message) {
		super(ex.getCode(), ex.getMessage() + " " + message, cause);
	}

	public RpcException(RPC_EXPECTION ex, String message) {
		super(ex.getCode(), ex.getMessage() + " " + message);
	}

	public RpcException(RPC_EXPECTION ex, Throwable cause) {
		super(ex.getCode(), ex.getMessage(), cause);
	}

	public RpcException(RPC_EXPECTION ex) {
		super(ex.getCode(), ex.getMessage());
	}

	public RpcException(int code, String message) {
		super(code, message);
	}

	public RpcException(String message, Throwable cause) {
		super(BaseException.ERROR_CODE, message, cause);
	}

	public RpcException(String message) {
		super(BaseException.ERROR_CODE, message);
	}

	public RpcException() {
		super();
	}

	public enum RPC_EXPECTION {
		RPC_INVOK_EXCEPTION(100000, "服务调用异常!"),
		RPC_NOT_FOUND_SERVICE(100001, "无服务可以调用!"),
		RPC_NOT_IMPL(100002,"服务无实现类!");
		private RPC_EXPECTION(int code, String message) {
			this.code = code;
			this.message = message;
		}

		private int code;
		private String message;

		public int getCode() {
			return code;
		}

		public String getMessage() {
			return message;
		}
	}
}
