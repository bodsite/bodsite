package com.bodsite.rpc.spring.schema.bean;

/**
 * 服务
 * @author bod
 * 2017年4月13日上午10:33:55
 */
public class Service<T> {
    private String id;
	private Class<?> iface;//接口
	private Class<?> processor;//处理器
	// 接口实现类引用
    private T impl;
	private int port;//端口
	private int selectorThreads;//选择(读写)线程数 - 用于处理读写 I/O操作
	private int workerThreads;//工作业务线程数
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Class<?> getIface() {
		return iface;
	}
	public void setIface(Class<?> iface) {
		this.iface = iface;
	}
	public Class<?> getProcessor() {
		return processor;
	}
	public void setProcessor(Class<?> processor) {
		this.processor = processor;
	}
	public T getImpl() {
		return impl;
	}
	public void setImpl(T impl) {
		this.impl = impl;
	}
	public int getPort() {
		return port;
	}
	public void setPort(int port) {
		this.port = port;
	}
	public int getSelectorThreads() {
		return selectorThreads;
	}
	public void setSelectorThreads(int selectorThreads) {
		this.selectorThreads = selectorThreads;
	}
	public int getWorkerThreads() {
		return workerThreads;
	}
	public void setWorkerThreads(int workerThreads) {
		this.workerThreads = workerThreads;
	}
	@Override
	public String toString() {
		return "Service [id=" + id + ", iface=" + iface + ", processor=" + processor + ", impl=" + impl + ", port="
				+ port + ", selectorThreads=" + selectorThreads + ", workerThreads=" + workerThreads + "]";
	}

}
