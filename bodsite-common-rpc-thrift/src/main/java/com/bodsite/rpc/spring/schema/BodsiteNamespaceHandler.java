package com.bodsite.rpc.spring.schema;

import org.springframework.beans.factory.xml.NamespaceHandlerSupport;

import com.bodsite.rpc.spring.schema.bean.Invok;
import com.bodsite.rpc.spring.schema.bean.Module;
import com.bodsite.rpc.spring.schema.bean.Service;
import com.bodsite.rpc.spring.schema.bean.Zookeeper;

public class BodsiteNamespaceHandler extends NamespaceHandlerSupport{

	@Override
	public void init() {
		registerBeanDefinitionParser("module", new BodsiteBeanDefinitionParser(Module.class));
		registerBeanDefinitionParser("zookeeper", new BodsiteBeanDefinitionParser(Zookeeper.class));
		registerBeanDefinitionParser("service", new BodsiteBeanDefinitionParser(Service.class));
		registerBeanDefinitionParser("invok", new BodsiteBeanDefinitionParser(Invok.class));
	}

}
