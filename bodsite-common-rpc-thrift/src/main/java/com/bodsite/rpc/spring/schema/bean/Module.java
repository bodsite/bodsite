package com.bodsite.rpc.spring.schema.bean;

/**
 * 模块服务名
 * @author bod
 * 2017年4月13日上午10:33:40
 */
public class Module {
    private String id;

	private String name;//名称
	
	private String version;//版本

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	@Override
	public String toString() {
		return "Module [id=" + id + ", name=" + name + ", version=" + version + "]";
	}

}
