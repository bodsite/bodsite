package com.bodsite.rpc.spring.schema.bean;

/**
 * 调用
 * @author bod
 * 2017年4月13日上午10:33:30
 */
public class Invok {
	private String id;
	//代理类client
	private Class<?> client;
	//接口
	private Class<?> iface;
	
	public Class<?> getIface() {
		return iface;
	}
	public void setIface(Class<?> iface) {
		this.iface = iface;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Class<?> getClient() {
		return client;
	}
	public void setClient(Class<?> client) {
		this.client = client;
	}
	

}
