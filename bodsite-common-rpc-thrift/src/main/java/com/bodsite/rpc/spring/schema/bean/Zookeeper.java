package com.bodsite.rpc.spring.schema.bean;

/**
 * 注册中心zk配置
 * @author bod
 * 2017年4月13日上午10:34:01
 */
public class Zookeeper {
    private String id;
	private String address;
	private String group;
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	@Override
	public String toString() {
		return "Zookeeper [id=" + id + ", address=" + address + ", group=" + group + "]";
	}

}
