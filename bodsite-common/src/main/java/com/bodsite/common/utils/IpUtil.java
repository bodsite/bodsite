package com.bodsite.common.utils;

import java.net.InetAddress;
import java.net.UnknownHostException;

import com.bodsite.common.logger.Logger;
import com.bodsite.common.logger.LoggerFactory;


public class IpUtil {
	private static final Logger logger = LoggerFactory.getLogger(IpUtil.class);

	public static String getLocalIp() {
		try {
			InetAddress ia = InetAddress.getLocalHost();
			return ia.getHostAddress().toString();
		} catch (UnknownHostException e) {
			logger.error(e.getMessage(), e);
		}
		return "";
	}

}
