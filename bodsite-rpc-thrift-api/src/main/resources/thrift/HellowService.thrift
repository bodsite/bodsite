namespace java com.bodsite.rpc.thrift.service

service HellowService{
  string hellowString(1:string para);
  i32 hellowInt(1:i32 para);
  bool hellowBoolean(1:bool para); 
  void hellowVoid(); 
  string hellowNull(); 
}