namespace java com.bodsite.rpc.thrift.entity

struct OrderUserVo{
	1:i64 oid
	2:i64 uid
	3:string orderName
	4:string userName
}