namespace java com.bodsite.rpc.thrift.service

include "UserVo.thrift"
include "OrderVo.thrift"
include "OrderUserVo.thrift"
service UserService{
    void inserUserList(1:list<UserVo.UserVo> userList);
	void insertUserAndOrder(1:UserVo.UserVo userVo,2:OrderVo.OrderVo orderVo);
	void insertUser(1:UserVo.UserVo userVo);
	void updateUser(1:UserVo.UserVo userVo);
	UserVo.UserVo findUser(1:i64 userId);
	void insertOrder(1:OrderVo.OrderVo orderVo);
	void updateOrder(1:OrderVo.OrderVo orderVo);
	OrderVo.OrderVo findOrder(1:i64 orderId, 2:i64 userId);
	list<OrderVo.OrderVo> findOrderList(1:OrderVo.OrderVo orderVo,2:i32 start,3:i32 size);
	list<OrderUserVo.OrderUserVo> findOrderUser(1:OrderUserVo.OrderUserVo orderUserVo,2:i32 start,3:i32 size);  
}