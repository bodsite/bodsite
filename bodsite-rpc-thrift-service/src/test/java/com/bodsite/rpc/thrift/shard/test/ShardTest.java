package com.bodsite.rpc.thrift.shard.test;

import java.util.ArrayList;
import java.util.List;

import org.apache.thrift.TException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.bodsite.common.logger.Logger;
import com.bodsite.common.logger.LoggerFactory;
import com.bodsite.rpc.thrift.entity.OrderUserVo;
import com.bodsite.rpc.thrift.entity.OrderVo;
import com.bodsite.rpc.thrift.entity.UserVo;
import com.bodsite.rpc.thrift.service.UserService;
import com.bodsite.rpc.thrift.service.impl.UserServiceImpl;
import com.dangdang.ddframe.rdb.sharding.id.generator.self.IPIdGenerator;

/**
 * dangdang shard-jdbc 分库分表 测试
 * 需先执行 /src/main/resources/bodsite.sql
 * @author bod
 * 2017年4月24日下午5:02:38
 */
public class ShardTest {
	private final static Logger logger = LoggerFactory.getLogger(ShardTest.class);
	public static void main(String[] args) throws TException {
		ApplicationContext ac = new ClassPathXmlApplicationContext("spring/application-service.xml");
		UserService.Iface userService = ac.getBean(UserServiceImpl.class);
		insertShardDatabaseTransactional(userService);
	}
	
	/**
	 * 分库事务测试
	 * @author bod
	 * 2017年4月28日上午10:59:32
	 */
	public static void insertShardDatabaseTransactional(UserService.Iface userService) throws TException{
		List<UserVo> list = new ArrayList<UserVo>();
		UserVo user = new UserVo();
		Long uid = 64423161434746880L;//库1  
		user.setUid(uid);
		user.setName("user-"+1);
		UserVo user2 = new UserVo();
		Long uid2 = 64423616407678976L;
		user2.setUid(uid2);
		user2.setName("user-"+2);//库2
		list.add(user);
		list.add(user2);
		userService.inserUserList(list);
	}
	
	/**
	 * 单库事务测试
	 * @author bod
	 * 2017年4月28日上午10:59:41
	 */
	public static void insertTransactional(UserService.Iface userService) throws TException{
		UserVo user = new UserVo();
		OrderVo order = new OrderVo();
		IPIdGenerator idGen = new IPIdGenerator();
		Long uid = idGen.generateId().longValue();//分布式id生成 
		user.setUid(uid);
		user.setName("user-"+1);
		Long oid = idGen.generateId().longValue();
		order.setOid(oid);
		order.setUid(uid);
		order.setName("order-"+0+"-"+0);
		userService.insertUserAndOrder(user, order);
	}
	
	/**
	 * 插入测试
	 * @author bod
	 * 2017年4月28日上午11:00:08
	 */
	public static void insertTest(UserService.Iface userService) throws TException{
		UserVo user;
		OrderVo order;
		IPIdGenerator idGen = new IPIdGenerator();
		for(int i=0;i<50;i++){
			user = new UserVo();
			Long uid = idGen.generateId().longValue();//分布式id生成 
			logger.info(uid.toString());
			user.setUid(uid);
			user.setName("user-"+i);
			userService.insertUser(user);
			for(int j=0;j<5;j++){
				order =new OrderVo();
				Long oid = idGen.generateId().longValue();
				order.setOid(oid);
				order.setUid(uid);
				order.setName("order-"+i+"-"+j);
				userService.insertOrder(order);
			}
		}
	}
	
	/**
	 * 查询测试
	 * @author bod
	 * 2017年4月28日上午11:00:18
	 */
	public static void findTest(UserService.Iface userService,Long uesrId) throws TException{
		UserVo user1 = userService.findUser(uesrId);
		logger.info(user1.toString());
	}
	
	/**
	 * 分页查询测试
	 * @author bod
	 * 2017年4月28日上午11:00:25
	 */
	//注意分表分页，如果没有分表id 将是全表查找，并且每片都将从0到当前最大条数进行查找，合并，分页，效率太低，尽量不要这样查询！
	public static void find4Limit(UserService.Iface userService) throws TException{
		OrderVo orderf = new OrderVo();
		List<OrderVo> orderList = userService.findOrderList(orderf,0,20);
		if(orderList!=null&&!orderList.isEmpty()){
			for(OrderVo o:orderList){
				logger.info(o.toString());
			}
			logger.info("size:"+orderList.size());
		}
	}
	
	/**
	 * 绑定表关联查询测试
	 * @author bod
	 * 2017年4月28日上午11:00:38
	 */
	public static void find4Join(UserService.Iface userService) throws TException{
		OrderUserVo orderUserf = new OrderUserVo();
		List<OrderUserVo> orderUserList = userService.findOrderUser(orderUserf,0,20);
		if(orderUserList!=null&&!orderUserList.isEmpty()){
			for(OrderUserVo orderUser:orderUserList){
				logger.info(orderUser.toString());
			}
			logger.info("size:"+orderUserList.size());
		}
	}

}
