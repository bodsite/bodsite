package com.bodsite.rpc.thrift.service.impl;

import org.apache.thrift.TException;
import org.springframework.stereotype.Service;

import com.bodsite.rpc.thrift.service.HellowService;

@Service
public class HellowServiceImpl implements HellowService.Iface {

	@Override
	public String hellowString(String para) throws TException {
		System.out.println("server:"+para);
		return para;
	}

	@Override
	public int hellowInt(int para) throws TException {
		try {
			Thread.sleep(20000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return para;
	}

	@Override
	public boolean hellowBoolean(boolean para) throws TException {
		return para;
	}

	@Override
	public void hellowVoid() throws TException {
		System.out.println("hello world");
	}

	@Override
	public String hellowNull() throws TException {
		return null;
	}



}
