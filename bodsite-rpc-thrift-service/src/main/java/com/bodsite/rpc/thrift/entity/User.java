package com.bodsite.rpc.thrift.entity;

/**
 * 用户
 * @author bod
 * 2017年4月20日下午1:26:24
 */
public class User {
	private Long uid;
	private String name;
	public Long getUid() {
		return uid;
	}
	public void setUid(Long uid) {
		this.uid = uid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Override
	public String toString() {
		return "User [uid=" + uid + ", name=" + name + "]";
	}
}
