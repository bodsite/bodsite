package com.bodsite.rpc.thrift.dao;

import org.apache.ibatis.annotations.Param;

import com.bodsite.rpc.thrift.entity.User;

public interface UserMapper {
    int deleteByPrimaryKey(@Param("uid")Long uid);

    int insert(User user);

    int insertSelective(User user);

    User selectByPrimaryKey(@Param("uid")Long uid);

    int updateByPrimaryKeySelective(User user);

    int updateByPrimaryKey(User user);
}