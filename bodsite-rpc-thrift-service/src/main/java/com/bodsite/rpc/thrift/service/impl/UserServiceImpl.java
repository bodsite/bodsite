package com.bodsite.rpc.thrift.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.thrift.TException;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bodsite.common.cache.annotation.Cache;
import com.bodsite.common.constant.CacheConstant.CACHE_TYPE;
import com.bodsite.common.constant.LockConstant.LOCK_TYPE;
import com.bodsite.common.lock.annotation.Lock;
import com.bodsite.common.logger.Logger;
import com.bodsite.common.logger.LoggerFactory;
import com.bodsite.rpc.thrift.dao.OrderMapper;
import com.bodsite.rpc.thrift.dao.UserMapper;
import com.bodsite.rpc.thrift.entity.Order;
import com.bodsite.rpc.thrift.entity.OrderUser;
import com.bodsite.rpc.thrift.entity.OrderUserVo;
import com.bodsite.rpc.thrift.entity.OrderVo;
import com.bodsite.rpc.thrift.entity.User;
import com.bodsite.rpc.thrift.entity.UserVo;
import com.bodsite.rpc.thrift.service.UserService;

@Service
public class UserServiceImpl implements UserService.Iface{
	private Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);
	@Autowired
	private OrderMapper orderMapper;
	@Autowired
	private UserMapper userMapper;
	
	@Override
	@Transactional(propagation=Propagation.REQUIRED,readOnly=false,rollbackFor=Exception.class,timeout=5000)
	public void insertUserAndOrder(UserVo userVo,OrderVo orderVo) throws TException{
		insertUser(userVo);
		insertOrder(orderVo);
		int i=1/0;
	}
	
	@Override
	@Transactional(propagation=Propagation.REQUIRED,readOnly=false,rollbackFor=Exception.class,timeout=10000)
	public void inserUserList(List<UserVo> userList) throws TException{
		for(UserVo user:userList){
			insertUser(user);
		}
		int i=1/0;
	}
	
	@Override
	public void insertUser(UserVo userVo) throws TException{
		try{
			User user = new User();
			BeanUtils.copyProperties(userVo, user);
			userMapper.insertSelective(user);
		}catch(Exception e){
			throw new TException(e);
		}
	}
	
	@Override
	public void updateUser(UserVo userVo) throws TException{
		try{
			User user = new User();
			BeanUtils.copyProperties(userVo, user);
			userMapper.updateByPrimaryKeySelective(user);
		}catch(Exception e){
			throw new TException(e);
		}
	}
	
	@Override
	public UserVo findUser(long userId) throws TException{
		try{
			User user = userMapper.selectByPrimaryKey(userId);
			UserVo userVo = null;
			if(user!=null){
				userVo = new UserVo();
				BeanUtils.copyProperties(user, userVo);
			}
			return userVo;
		}catch(Exception e){
			throw new TException(e);
		}
	}
	
	@Override
	public void insertOrder(OrderVo orderVo) throws TException{
		try{
			Order order = new Order();
			BeanUtils.copyProperties(orderVo, order);
			orderMapper.insertSelective(order);
		}catch(Exception e){
			throw new TException(e);
		}
	}
	
	@Override
	public void updateOrder(OrderVo orderVo) throws TException{
		try{
			Order order = new Order();
			BeanUtils.copyProperties(orderVo, order);
			orderMapper.updateByPrimaryKeySelective(order);
		}catch(Exception e){
			throw new TException(e);
		}
	}
	
	@Override
	public OrderVo findOrder(long orderId,long userId) throws TException{
		try{
			Order order = orderMapper.selectByPrimaryKey(orderId,userId);
			OrderVo orderVo = new OrderVo();
			BeanUtils.copyProperties(order, orderVo);
			return orderVo;
		}catch(Exception e){
			throw new TException(e);
		}
	}
	
	@Override
	@Cache(key="order_list_{1}_{2}",cacheType=CACHE_TYPE.LOCAL_CACHE,exp=30)
	public List<OrderVo> findOrderList(OrderVo orderVo,int start,int size) throws TException{
		try{
			Order order = new Order();
			if(orderVo!=null){
				BeanUtils.copyProperties(orderVo, order);
			}
			List<Order> orders = orderMapper.selectList(order,start,size);
			List<OrderVo> orderVos = null;
			if(orders!=null && !orders.isEmpty()){
				orderVos = new ArrayList<OrderVo>();
				OrderVo oVo;
				for(Order o: orders){
					oVo = new OrderVo();
					BeanUtils.copyProperties(o, oVo);
					orderVos.add(oVo);
				}
			}
			return orderVos;
		}catch(Exception e){
			throw new TException(e);
		}
	}
	
	@Override
	public List<OrderUserVo> findOrderUser(OrderUserVo orderUserVo,int start,int size)throws TException{
		try{
			OrderUser orderUser = new OrderUser();
			if(orderUserVo!=null){
				BeanUtils.copyProperties(orderUserVo, orderUser);
			}
			List<OrderUser> orderUsers = orderMapper.selectOrderUserList(orderUser,start,size);
			List<OrderUserVo> orderUserVos = null;
			if(orderUsers!=null && !orderUsers.isEmpty()){
				orderUserVos = new ArrayList<OrderUserVo>();
				OrderUserVo ouVo;
				for(OrderUser o: orderUsers){
					ouVo = new OrderUserVo();
					BeanUtils.copyProperties(o, ouVo);
					orderUserVos.add(ouVo);
				}
			}
			return orderUserVos;
		}catch(Exception e){
			throw new TException(e);
		}
	}

}
