package com.bodsite.rpc.thrift.entity;

/**
 * 订单
 * @author bod
 * 2017年4月20日下午1:26:14
 */
public class Order {
	private Long oid;
	private Long uid;
	private String name;
	
	public Long getOid() {
		return oid;
	}
	public void setOid(Long oid) {
		this.oid = oid;
	}
	public Long getUid() {
		return uid;
	}
	public void setUid(Long uid) {
		this.uid = uid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Override
	public String toString() {
		return "Order [oid=" + oid + ", uid=" + uid + ", name=" + name + "]";
	}
}
