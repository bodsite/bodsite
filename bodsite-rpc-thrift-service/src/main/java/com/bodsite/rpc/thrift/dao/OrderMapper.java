package com.bodsite.rpc.thrift.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.bodsite.rpc.thrift.entity.Order;
import com.bodsite.rpc.thrift.entity.OrderUser;

public interface OrderMapper {
    int deleteByPrimaryKey(@Param("oid")Long oid);

    int insert(Order order);

    int insertSelective(Order order);

    Order selectByPrimaryKey(@Param("oid") Long oid,@Param("uid") Long uid);
    
    List<Order> selectList(@Param("order") Order order,@Param("start") Integer start,@Param("size") Integer size);

    int updateByPrimaryKeySelective(Order order);

    int updateByPrimaryKey(Order order);
    
    List<OrderUser> selectOrderUserList(@Param("orderUser") OrderUser orderUser,@Param("start") Integer start,@Param("size") Integer size);
}