package com.bodsite.rpc.thrift.entity;

/**
 * 订单-用户
 * @author bod
 * 2017年4月24日上午10:43:06
 */
public class OrderUser {
	private Long oid;
	private Long uid;
	private String orderName;//订单名称
	private String userName;//用户名称
	public Long getOid() {
		return oid;
	}
	public void setOid(Long oid) {
		this.oid = oid;
	}
	public Long getUid() {
		return uid;
	}
	public void setUid(Long uid) {
		this.uid = uid;
	}
	public String getOrderName() {
		return orderName;
	}
	public void setOrderName(String orderName) {
		this.orderName = orderName;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	@Override
	public String toString() {
		return "OrderUser [oid=" + oid + ", uid=" + uid + ", orderName=" + orderName + ", userName=" + userName + "]";
	}
}
