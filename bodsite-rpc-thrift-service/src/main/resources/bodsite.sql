CREATE DATABASE `bodsite0`;

USE `bodsite0`;

DROP TABLE IF EXISTS `t_user_0`;
CREATE TABLE `t_user_0`(
	`uid` bigint(20) NOT NULL COMMENT '用户id',
	`name` varchar(16) DEFAULT NULL COMMENT '名称',
	PRIMARY KEY (`uid`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户';

CREATE TABLE `t_user_1`(
	`uid` bigint(20) NOT NULL COMMENT '用户id',
	`name` varchar(16) DEFAULT NULL COMMENT '名称',
	PRIMARY KEY (`uid`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户';

CREATE TABLE `t_user_2`(
	`uid` bigint(20) NOT NULL COMMENT '用户id',
	`name` varchar(16) DEFAULT NULL COMMENT '名称',
	PRIMARY KEY (`uid`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户';


CREATE TABLE `t_user_3`(
	`uid` bigint(20) NOT NULL COMMENT '用户id',
	`name` varchar(16) DEFAULT NULL COMMENT '名称',
	PRIMARY KEY (`uid`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户';


CREATE TABLE `t_user_4`(
	`uid` bigint(20) NOT NULL COMMENT '用户id',
	`name` varchar(16) DEFAULT NULL COMMENT '名称',
	PRIMARY KEY (`uid`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户';


CREATE TABLE `t_user_5`(
	`uid` bigint(20) NOT NULL COMMENT '用户id',
	`name` varchar(16) DEFAULT NULL COMMENT '名称',
	PRIMARY KEY (`uid`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户';


CREATE TABLE `t_user_6`(
	`uid` bigint(20) NOT NULL COMMENT '用户id',
	`name` varchar(16) DEFAULT NULL COMMENT '名称',
	PRIMARY KEY (`uid`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户';


CREATE TABLE `t_user_7`(
	`uid` bigint(20) NOT NULL COMMENT '用户id',
	`name` varchar(16) DEFAULT NULL COMMENT '名称',
	PRIMARY KEY (`uid`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户';


CREATE TABLE `t_user_8`(
	`uid` bigint(20) NOT NULL COMMENT '用户id',
	`name` varchar(16) DEFAULT NULL COMMENT '名称',
	PRIMARY KEY (`uid`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户';


CREATE TABLE `t_user_9`(
	`uid` bigint(20) NOT NULL COMMENT '用户id',
	`name` varchar(16) DEFAULT NULL COMMENT '名称',
	PRIMARY KEY (`uid`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户';



CREATE TABLE `t_order_0`(
	`oid` bigint(20) NOT NULL COMMENT '订单id',
	`uid` bigint(20) NOT NULL COMMENT '用户id',
	`name` varchar(16) DEFAULT NULL COMMENT '名称',
	PRIMARY KEY (`oid`,`uid`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='订单';

CREATE TABLE `t_order_1`(
	`oid` bigint(20) NOT NULL COMMENT '订单id',
	`uid` bigint(20) NOT NULL COMMENT '用户id',
	`name` varchar(16) DEFAULT NULL COMMENT '名称',
	PRIMARY KEY (`oid`,`uid`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='订单';

CREATE TABLE `t_order_2`(
	`oid` bigint(20) NOT NULL COMMENT '订单id',
	`uid` bigint(20) NOT NULL COMMENT '用户id',
	`name` varchar(16) DEFAULT NULL COMMENT '名称',
	PRIMARY KEY (`oid`,`uid`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='订单';


CREATE TABLE `t_order_3`(
	`oid` bigint(20) NOT NULL COMMENT '订单id',
	`uid` bigint(20) NOT NULL COMMENT '用户id',
	`name` varchar(16) DEFAULT NULL COMMENT '名称',
	PRIMARY KEY (`oid`,`uid`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='订单';

CREATE TABLE `t_order_4`(
	`oid` bigint(20) NOT NULL COMMENT '订单id',
	`uid` bigint(20) NOT NULL COMMENT '用户id',
	`name` varchar(16) DEFAULT NULL COMMENT '名称',
	PRIMARY KEY (`oid`,`uid`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='订单';

CREATE TABLE `t_order_5`(
	`oid` bigint(20) NOT NULL COMMENT '订单id',
	`uid` bigint(20) NOT NULL COMMENT '用户id',
	`name` varchar(16) DEFAULT NULL COMMENT '名称',
	PRIMARY KEY (`oid`,`uid`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='订单';

CREATE TABLE `t_order_6`(
	`oid` bigint(20) NOT NULL COMMENT '订单id',
	`uid` bigint(20) NOT NULL COMMENT '用户id',
	`name` varchar(16) DEFAULT NULL COMMENT '名称',
	PRIMARY KEY (`oid`,`uid`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='订单';

CREATE TABLE `t_order_7`(
	`oid` bigint(20) NOT NULL COMMENT '订单id',
	`uid` bigint(20) NOT NULL COMMENT '用户id',
	`name` varchar(16) DEFAULT NULL COMMENT '名称',
	PRIMARY KEY (`oid`,`uid`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='订单';

CREATE TABLE `t_order_8`(
	`oid` bigint(20) NOT NULL COMMENT '订单id',
	`uid` bigint(20) NOT NULL COMMENT '用户id',
	`name` varchar(16) DEFAULT NULL COMMENT '名称',
	PRIMARY KEY (`oid`,`uid`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='订单';

CREATE TABLE `t_order_9`(
	`oid` bigint(20) NOT NULL COMMENT '订单id',
	`uid` bigint(20) NOT NULL COMMENT '用户id',
	`name` varchar(16) DEFAULT NULL COMMENT '名称',
	PRIMARY KEY (`oid`,`uid`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='订单';

CREATE DATABASE `bodsite1`;

USE `bodsite1`;

CREATE TABLE `t_user_10`(
	`uid` bigint(20) NOT NULL COMMENT '用户id',
	`name` varchar(16) DEFAULT NULL COMMENT '名称',
	PRIMARY KEY (`uid`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户';

CREATE TABLE `t_user_11`(
	`uid` bigint(20) NOT NULL COMMENT '用户id',
	`name` varchar(16) DEFAULT NULL COMMENT '名称',
	PRIMARY KEY (`uid`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户';

CREATE TABLE `t_user_12`(
	`uid` bigint(20) NOT NULL COMMENT '用户id',
	`name` varchar(16) DEFAULT NULL COMMENT '名称',
	PRIMARY KEY (`uid`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户';

CREATE TABLE `t_user_13`(
	`uid` bigint(20) NOT NULL COMMENT '用户id',
	`name` varchar(16) DEFAULT NULL COMMENT '名称',
	PRIMARY KEY (`uid`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户';

CREATE TABLE `t_user_14`(
	`uid` bigint(20) NOT NULL COMMENT '用户id',
	`name` varchar(16) DEFAULT NULL COMMENT '名称',
	PRIMARY KEY (`uid`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户';

CREATE TABLE `t_user_15`(
	`uid` bigint(20) NOT NULL COMMENT '用户id',
	`name` varchar(16) DEFAULT NULL COMMENT '名称',
	PRIMARY KEY (`uid`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户';

CREATE TABLE `t_user_16`(
	`uid` bigint(20) NOT NULL COMMENT '用户id',
	`name` varchar(16) DEFAULT NULL COMMENT '名称',
	PRIMARY KEY (`uid`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户';

CREATE TABLE `t_user_17`(
	`uid` bigint(20) NOT NULL COMMENT '用户id',
	`name` varchar(16) DEFAULT NULL COMMENT '名称',
	PRIMARY KEY (`uid`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户';

CREATE TABLE `t_user_18`(
	`uid` bigint(20) NOT NULL COMMENT '用户id',
	`name` varchar(16) DEFAULT NULL COMMENT '名称',
	PRIMARY KEY (`uid`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户';

CREATE TABLE `t_user_19`(
	`uid` bigint(20) NOT NULL COMMENT '用户id',
	`name` varchar(16) DEFAULT NULL COMMENT '名称',
	PRIMARY KEY (`uid`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户';


CREATE TABLE `t_order_10`(
	`oid` bigint(20) NOT NULL COMMENT '订单id',
	`uid` bigint(20) NOT NULL COMMENT '用户id',
	`name` varchar(16) DEFAULT NULL COMMENT '名称',
	PRIMARY KEY (`oid`,`uid`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='订单';


CREATE TABLE `t_order_11`(
	`oid` bigint(20) NOT NULL COMMENT '订单id',
	`uid` bigint(20) NOT NULL COMMENT '用户id',
	`name` varchar(16) DEFAULT NULL COMMENT '名称',
	PRIMARY KEY (`oid`,`uid`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='订单';


CREATE TABLE `t_order_12`(
	`oid` bigint(20) NOT NULL COMMENT '订单id',
	`uid` bigint(20) NOT NULL COMMENT '用户id',
	`name` varchar(16) DEFAULT NULL COMMENT '名称',
	PRIMARY KEY (`oid`,`uid`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='订单';



CREATE TABLE `t_order_13`(
	`oid` bigint(20) NOT NULL COMMENT '订单id',
	`uid` bigint(20) NOT NULL COMMENT '用户id',
	`name` varchar(16) DEFAULT NULL COMMENT '名称',
	PRIMARY KEY (`oid`,`uid`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='订单';



CREATE TABLE `t_order_14`(
	`oid` bigint(20) NOT NULL COMMENT '订单id',
	`uid` bigint(20) NOT NULL COMMENT '用户id',
	`name` varchar(16) DEFAULT NULL COMMENT '名称',
	PRIMARY KEY (`oid`,`uid`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='订单';


CREATE TABLE `t_order_15`(
	`oid` bigint(20) NOT NULL COMMENT '订单id',
	`uid` bigint(20) NOT NULL COMMENT '用户id',
	`name` varchar(16) DEFAULT NULL COMMENT '名称',
	PRIMARY KEY (`oid`,`uid`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='订单';


CREATE TABLE `t_order_16`(
	`oid` bigint(20) NOT NULL COMMENT '订单id',
	`uid` bigint(20) NOT NULL COMMENT '用户id',
	`name` varchar(16) DEFAULT NULL COMMENT '名称',
	PRIMARY KEY (`oid`,`uid`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='订单';


CREATE TABLE `t_order_17`(
	`oid` bigint(20) NOT NULL COMMENT '订单id',
	`uid` bigint(20) NOT NULL COMMENT '用户id',
	`name` varchar(16) DEFAULT NULL COMMENT '名称',
	PRIMARY KEY (`oid`,`uid`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='订单';


CREATE TABLE `t_order_18`(
	`oid` bigint(20) NOT NULL COMMENT '订单id',
	`uid` bigint(20) NOT NULL COMMENT '用户id',
	`name` varchar(16) DEFAULT NULL COMMENT '名称',
	PRIMARY KEY (`oid`,`uid`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='订单';


CREATE TABLE `t_order_19`(
	`oid` bigint(20) NOT NULL COMMENT '订单id',
	`uid` bigint(20) NOT NULL COMMENT '用户id',
	`name` varchar(16) DEFAULT NULL COMMENT '名称',
	PRIMARY KEY (`oid`,`uid`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='订单';




